本地端建立專案，測試上傳Git成功
我的gitlab username 是 @shijjjung 建立一個project 叫做 testGit
1. git 初始化
    ```
    git init
    ```
2. 設定 git master 主分支
ex: git remote add branchName url
以下為正確指定

    ```
    git remote add -m master https://gitlab.com/shijjjung/testGit
    git remote add master https://github.com/shijjjung/testGit.git
    ```

3. 設定 git origin 遠端分支 就把 master 改 origin

    ```
    git remote add -m origin https://gitlab.com/shijjjung/testGit
    git remote add origin https://github.com/shijjjung/testGit.git
    ```

4. 設定完畢之後要commit 要上去的資料 舉例要把README.md 推上去

    ```
    git add README.md
    ``` 
4. [補充]如果是要推上全部 
    ```
    git add .
    ```
5. 將本次新增內容 建立一Commit 到 該分支上`這邊的(-m):--message 是訊息的意思` 另外確認當前分支與切換於補充說明指令

- 通常commit命名前面會使用 
    1. 新增[new]
    2. 更新[feat]
    3. 修改[fix]
    4. 刪除[delete]
等[動作名稱]:[描述內容]
    ```
    git commit -m "new: add README.md file"
    ```
6. 推上origin master

    ```
    git push origin master
    ``` 


## 補充 remote 設定錯誤刪除方式與查詢

- 查詢 remote 
    ```
    git remote -v

    master  https://gitlab.com/shijjjung/testGit (fetch)

    master  https://gitlab.com/shijjjung/testGit (push)

    origin  https://gitlab.com/shijjjung/testGit (fetch)

    origin  https://gitlab.com/shijjjung/testGit (push)
    ```
- 假設錯誤設定的分支 errorbranch 需要刪除
    ```
    git remote remove errorbranch
    ```
    ~~errorbranch https://gitlab.com/shijjjung/testGit (fetch)~~

    ~~errorbranch https://gitlab.com/shijjjung/testGit (push)~~

- 指令可以到git docs 去查詢 以下為簡單幾項目
    ```
    git status
    查詢狀態以及有無未推上的 commit
    On branch xxx<= 當前所在分支
    
    git branch
    * 為當前分支(按下q可以離開)

    git branch 相關新刪修查可以查一下doc
    https://git-scm.com/docs/git-branch
    ```

- 分支切換測試

    1. 新增一分支 develop

        ```
        git branch develop
        ```
    2. 切換分支過去
        ```
        git checkout develop
        ```
    3. git status 查看有沒有更動的檔案

        `modified:   xxx`
    4. 新分支新推上
        ```
        git add .
        git commit -m "feat: 補充切換分支內容"
        git push origin master
        ```
    5. 檢查 master 與 develop 不同
        1. 回到 master
        ```
        git checkout master
        ```
        *** `無法回到master` 可能有錯誤訊息 當前分支有更動的地方沒有推上 但還沒要推上時可以使用暫存 stash
        ```
        git stash save "可省略也可註解的訊息"
        ```
        再次執行步驟1
        2. 查看差異 並merge
        ```
        git diff master..develop
        git merge develop
        ```
        3. 把暫存丟回來 
        ```
        git stash list 查詢目前有的暫存 stashID 0,1,2
        git stash pop 0
        ```
        4. 看 stash 的內容 要在哪邊進行 commit 推上再如上進行切換
        5. stash 清除
        ```
        git stash drop 0
        ```
- 重做變動需要reset的部分
1. git status 可以查看 
    
    Changes not staged for commit:

        (use "git restore --staged <file>..." to unstage)
        (use "git restore <file>..." to discard changes in working directory)
                modified:   README.md
2. 已經 stage 的東西要取消 
    ```
    git restore --staged <fileName>
    ```
2. 未 stage 的東西要 discard changes 還原變動 
    ```
    git restore <fileName>
    ```
